#!/usr/bin/env python3
import board
import neopixel
import time
import configparser
import json
import VL53L0X

config = configparser.ConfigParser()
config.read('/boot/bling-range.ini')

# Create a VL53L0X object
tof = VL53L0X.VL53L0X()
# Start ranging
tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

# Setup NeoPixel
pixels = neopixel.NeoPixel(board.D18, 25, brightness=int(config['DEFAULT']['BRIGHTNESS'])/100, auto_write=False, pixel_order=neopixel.GRB)

OOR_DELAY = int(config['DEFAULT']['OOR_DELAY'])/1000
TARGET_DISTANCE = int(config['DEFAULT']['TARGET_DISTANCE'])
TARGET_RANGE = int(config['DEFAULT']['TARGET_RANGE'])
MIN_DISTANCE_CLOSE = int(config['DEFAULT']['MIN_DISTANCE_CLOSE'])
MAX_DISTANCE_FAR = int(config['DEFAULT']['MAX_DISTANCE_FAR'])

TARGET_LED = int(config['DEFAULT']['TARGET_LED'])
MAX_LED = int(config['DEFAULT']['MAX_LED'])
MIN_LED = int(config['DEFAULT']['MIN_LED'])

OUT_OF_RANGE_COLOR = tuple([int(x) for x in config['DEFAULT']['OUT_OF_RANGE_COLOR'].split(',')])
FAR_COLOR = tuple([int(x) for x in config['DEFAULT']['FAR_COLOR'].split(',')])
CLOSE_COLOR = tuple([int(x) for x in config['DEFAULT']['CLOSE_COLOR'].split(',')])
TARGET_COLOR = tuple([int(x) for x in config['DEFAULT']['TARGET_COLOR'].split(',')])

def light_up_distance(distance, previous_val):
  # If the distance is too close light up scaled number of pixels to center.
  if distance > MAX_DISTANCE_FAR:
    if previous_val == -1:
      return -1
    pixels.fill((0,0,0))
    pixels.fill(OUT_OF_RANGE_COLOR)
    pixels.show()


#    print('distance OORange: {}'.format(distance))
    return previous_val
  if distance + TARGET_RANGE < TARGET_DISTANCE:
    x = MAX_LED - int(( ((distance-MIN_DISTANCE_CLOSE) * (MAX_LED-TARGET_LED)) /
		(TARGET_DISTANCE - MIN_DISTANCE_CLOSE) ))
#    print('distance NEAR: {}\nx: {}'.format(distance, x))
    if x == previous_val:
      return x
    pixels.fill((0,0,0))
    pixels[TARGET_LED:x] = [CLOSE_COLOR] * (x - TARGET_LED)
    pixels.show()
    return x
  if distance - TARGET_RANGE > TARGET_DISTANCE:
    x = TARGET_LED - int(( ((distance-TARGET_DISTANCE) * (TARGET_LED)) /
		(MAX_DISTANCE_FAR - TARGET_DISTANCE) ))-1
    if x < MIN_LED:
      x = MIN_LED
    if x == previous_val:
      return x
#    print('distance FAR: {}\nx: {}'.format(distance, x))
    pixels.fill((0,0,0))
    pixels[x:TARGET_LED] = [FAR_COLOR] * (TARGET_LED-x)
    pixels.show()
    return x
#  print('distance SUCCESS: {}\n'.format(distance))
  if previous_val == -2:
    return -2
  pixels.fill((0,0,0))
  pixels.fill(TARGET_COLOR)
  pixels.show()
  return -2

if __name__ == '__main__':
  previous_val = 0
  try:
    while True:
      d = tof.get_distance()
      #pixels.fill((0,0,0))
      previous_val = light_up_distance(d, previous_val)
  except KeyboardInterrupt:
    print('Bye')
