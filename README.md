# Range Bling
A visual feedback solution for object distance based on a raspberry pi zero, VL53L0X I2C lidar, and NeoPixel strips.
### Setup
Clone this repo onto the Pi Zero and install Python3 dependencies ***TODO* _Add requirements and wheels_**.
```
$ sudo apt install gitcore
$ git clone https://gitlab.com/DirtyJerz/range-bling.git
$ sudo python3 -m pip install -r requirements.txt
```
Move the config file ```range-bling.ini``` to the boot folder
```
$ cd range-bling
$ sudo mv range-bling.ini /boot/
```
Create a systemd service file in ```/lib/systemd/system```
```
$ sudo nano /lib/systemd/system/range-bling.service
```
Copy the contents below into the file.

if you did not clone the repo under the pi user's home folder, change ```/home/pi/range-bling``` to repo location.
```
[Unit]
Description=LED Proximity Feedback

[Service]
Type=idle
ExecStart=/usr/bin/python3 /home/pi/range-bling/main.py

[Install]
WantedBy=multi-user.target
```
Enable the service to start on boot
```
$ sudo systemctl enable range-bling.service
$ sudo systemctl daemon-reload
```
### Wiring Outline
Colors reflect 2019 version

(Left/Right are looking at the input of screw terminal)
```
LIDAR                   PI-Lidar (4 pos)
------                  ------
(VCC)-------BLUE-------(5V0) **LEFT SCREW TERMINAL
(GND)-------BLACK------(GND)
(SDA)-------WHITE------(SDA)
(SCL)-------BROWN------(SCL) **RIGHT SCREW TERMINAL
```
```
5 Volt PCB              PI-Power (2 pos)
------                  --------
(5V0)-------BLACK/RED---(5V0) **LEFT SCREW TERMINAL
(GND)-------BLACK-------(GND) **RIGHT SCREW TERMINAL
```
```
NeoPixels               PI-LEDs (3 pos)
---------               ------
(DIG)-------WHITE------(#18) **LEFT SCREW TERMINAL
(GND)-------BLACK------(GND)
(VCC)-------RED--------(5V0) **RIGHT SCREW TERMINAL
```
### Configuration
Carefully remove the SD card from the Pi and insert it into a PC. 

The /boot partition is FAT32 formatted will be visible and editable in Windows.

!!DO NOT FORMAT THE OTHER PARTITION IF WINDOWS ASKS YOU TO!!

Edit the values on the sd card in the file called range-bling.ini

